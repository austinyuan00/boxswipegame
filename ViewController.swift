//  Filename: ViewController.swift
//  EID: ahy259
//  Course: CS371L
//
//  Created by Austin Yuan on 7/6/20.
//  Copyright © 2020 Austin Yuan. All rights reserved.
//


import UIKit

let rows = 19
let columns = 9

class ViewController: UIViewController {

    var block: UIView!

    var screenWidth: CGFloat = UIScreen.main.bounds.width
    var screenHeight: CGFloat = UIScreen.main.bounds.height
    var blockWidth: CGFloat = UIScreen.main.bounds.width/CGFloat(columns)
    var blockHeight: CGFloat = UIScreen.main.bounds.height/CGFloat(rows)

    var newDirection: String = ""
    let changeDirection = DispatchSemaphore(value: 1)
    var gameOver: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        block = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: blockWidth , height: blockHeight))
        block.center = self.view.center
        block.backgroundColor = UIColor.green
        self.view.addSubview(block)

        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(recognizeTapGesture(recognize:)))
        self.view.addGestureRecognizer(tapRecognizer)
        
        let swipeRightRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(recognizeSwipeGesture(recognizer:)))
        swipeRightRecognizer.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRightRecognizer)
        
        let swipeLeftRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(recognizeSwipeGesture(recognizer:)))
        swipeLeftRecognizer.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeftRecognizer)
        
        let swipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(recognizeSwipeGesture(recognizer:)))
        swipeUpRecognizer.direction = UISwipeGestureRecognizer.Direction.up
        self.view.addGestureRecognizer(swipeUpRecognizer)
        
        let swipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(recognizeSwipeGesture(recognizer:)))
        swipeDownRecognizer.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDownRecognizer)
        
    }
    
    @IBAction func recognizeTapGesture(recognize: UITapGestureRecognizer) {
        // stop the user from "cheating" can't restart unless game is over
        if gameOver == false
        {
            return
        }
        block.center = self.view.center
        block.backgroundColor = UIColor.green
        
        self.changeDirection.wait()
        gameOver = false
        self.newDirection = "down"
        self.changeDirection.signal()
        
        runBackgroundThread(direction: "down")
        
    }

    @IBAction func recognizeSwipeGesture(recognizer: UISwipeGestureRecognizer) {
        
        var swipeDirection: String = ""
        
        switch recognizer.direction {
        case UISwipeGestureRecognizer.Direction.right:
            swipeDirection = "right"
        case UISwipeGestureRecognizer.Direction.left:
            swipeDirection = "left"
        case UISwipeGestureRecognizer.Direction.up:
            swipeDirection = "up"
        case UISwipeGestureRecognizer.Direction.down:
            swipeDirection = "down"
        default:
            break
        }
        changeDirection.wait()
        if gameOver || swipeDirection == newDirection {
            changeDirection.signal()
            return
        } else {
            changeDirection.signal()
            runBackgroundThread(direction: swipeDirection)
        }
    }
    
    func update(direction: String, position: CGFloat) {
        DispatchQueue.main.async {
            self.changeDirection.wait()
            if self.newDirection == direction {
                UIView.animate(withDuration: 0,
                               animations: {
                                if direction == "left" || direction == "right" {
                                    self.block.center.x = position
                                } else {
                                    self.block.center.y = position } },
                               completion: {finished in
                                if self.gameOver {
                                    self.block.backgroundColor = UIColor.red }})
            }
            self.changeDirection.signal()
        }
    }
    
    func runBackgroundThread(direction: String) {
        var curY: CGFloat = self.block.center.y
        var curX: CGFloat = self.block.center.x
        var newPosition: CGFloat!
        DispatchQueue.global(qos: .userInteractive).async {
            self.changeDirection.wait()
            self.newDirection = direction
            self.changeDirection.signal()
            
            while (self.gameOver == false) {
                self.changeDirection.wait()
                if self.newDirection != direction {
                    self.changeDirection.signal()
                    return
                } else {
                    self.changeDirection.signal()
                    usleep(300000)
                    
                    self.changeDirection.wait()
                    
                    switch direction {
                    case "right":
                        curX += self.blockWidth
                        if curX + self.blockWidth > self.screenWidth && self.newDirection == direction {
                            self.gameOver = true }
                        newPosition = curX
                    case "left":
                        curX -= self.blockWidth
                        if curX - self.blockWidth < 0 && self.newDirection == direction {
                            self.gameOver = true }
                        newPosition = curX
                    case "up":
                        curY -= self.blockHeight
                        if curY - self.blockHeight < 0 && self.newDirection == direction {
                            self.gameOver = true }
                        newPosition = curY
                    case "down":
                        curY += self.blockHeight
                        if curY + self.blockHeight > self.screenHeight && self.newDirection == direction {
                            self.gameOver = true }
                        newPosition = curY
                    default:
                        break
                    }
                    self.changeDirection.signal()
                    self.update(direction: direction, position: newPosition)
                }
            }
        }
    }

}
